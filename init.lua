-- Nodepicker for Minetest 0.4.17.1
-- A magic wand that adds to your inventory whichever node your pointing to.
-- Texture source = https://minecraft.novaskin.me/skin/6660543364464640/Magic-Wand

minetest.register_alias("nodepicker", "nodepicker:nodepicker")

minetest.register_on_newplayer(function(player)
        p_name = player:get_player_name()
	print("[nodepicker] giving 1 nodepicker to player")
	minetest.after(0.1, function()
		minetest.chat_send_player(p_name, "Giving 1 nodepicker to " .. p_name)
	end)
		player:get_inventory():add_item('main', 'nodepicker:nodepicker')
end)

minetest.register_craft({
	output = 'nodepicker:nodepicker',
	recipe = {
		{'default:gold_lump'},
		{'default:stick'},
		{'default:stick'},
	}
})

minetest.register_tool("nodepicker:nodepicker", {
	description = "Nodepicker",
	inventory_image = "nodepicker.png",
	stack_max = 1,
	liquids_pointable = true,
	on_use = function(itemstack, user, pointed_thing)
		-- Pick nodes only
		if pointed_thing.type ~= "node" then
			return
		end
		local pointed_object = minetest.get_node(pointed_thing.under)
		local node_name = pointed_object.name
		local inv = user:get_inventory()
	 	if not inv:contains_item("main", node_name, true) then -- Avoid duplicates
                	inv:add_item("main", node_name)
			return 'nodepicker:nodepicker'
		end
	end
})
		

