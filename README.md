# Nodepicker
______

![Nodepicker](screenshot.png "It's a shitty wand!")

Nodepicker is a minetest 0.4.17+ mod that helps you build faster by letting you 
pick nodes from your surroundings and adding them to your inventory.

#### How it works:
  - You get 1 nodepicker when you first join a world
  - Left-click a node to add to your inventory/belt
  - Inventory items will deplete unless you are on creative mode

#### Installation:
extract zip to:

    Linux: ~/.minetest/mods/
    Windows (portable example): c:\Users\johnny\Downloads\minetest-0.4.17.1-win64\mods\
    MacOS: ~/Library/Application\ Support/minetest/mods/

#### TODO:
 - Use an original texture
 - Test it with games other than minetest_game
